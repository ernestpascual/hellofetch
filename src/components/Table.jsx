import React, { Fragment } from 'react';

const Table = (props) => 
    (
    <div>
      <table>
            <thead>
            <tr>
            <Fragment>
            {props.headers.map(item => (
            <th>{item.hd}</th>    
            ))}
            </Fragment>
            </tr>
            </thead>
            <tbody>
            <Fragment>

            {props.items.map(item => (
            <tr>
                <td>{item.name}</td>
                <td>{item.email}</td>
                <td>{item.address.geo.lat}</td>
                <td>{item.company.name}</td>
                <td> <button onClick={props.hello} > Hello! </button> </td>
            
            </tr>
            ))}
            </Fragment>
            </tbody>
       </table>
    </div>
    );


export default Table;