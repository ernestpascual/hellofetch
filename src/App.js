import React, { Component } from 'react';
import './App.css';
import Table from './components/Table'


class App extends Component {
  state={
    headers: [{hd: "Name"}, {hd: "Email"}, {hd: "Suite"}, {hd: "Company"}, {hd: "taga hello"}],
    users: []
  }

  componentWillMount(){
   fetch('http://jsonplaceholder.typicode.com/users') // GET Request on the URL
   .then(response => response.json()) // ano ni response ni server => JSON Object
    .then(json => {
      this.setState({
        users: json
      })
      console.log(this.state.users)
    })

    

  }

  hello = event => {
    alert('HELLO')
  }
 
  render() {
    return (
      <div>
        <h1> Users </h1>
        <Table headers={this.state.headers} items={this.state.users} hello={this.hello} />
      </div>
    );
  }
}

export default App;

// https://css-tricks.com/snippets/css/a-guide-to-flexbox/